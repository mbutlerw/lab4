package ie.ucd.events;

import java.util.ArrayList;
import ie.ucd.people.Person;

public abstract class Event {
	private ArrayList<Person> participants;

	public Event(ArrayList<Person> participants) 
	{
		// Generate event
		this.participants = participants;
	}

	public abstract boolean isSuccessful();

	public ArrayList<Person> getParticipants() 
	{
		// Return attendees
		return participants;
	}

	public void setParticipants(ArrayList<Person> participants) 
	{
		// Define attendees
		this.participants = participants;
	}
}
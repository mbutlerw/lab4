package ie.ucd.events;

import java.util.ArrayList;

import ie.ucd.people.*;

public class DrinkingEvent extends Event {

	public DrinkingEvent(ArrayList<Person> participants) 
	{
		super(participants);
		// Does the same thing as Event
		// Not strictly necessary to include this part,
		// but useful to remember that the function is here
	}

	@Override
	public boolean isSuccessful() {
		if (this.getParticipants().isEmpty()) {
			// No one here, ignore this case
			return false;
		}
		else {
			// Some amount of people here
			float people = 0;
			float drunk = 0;
			float female = 0;
			for (Person p : this.getParticipants()) {
				people++;
				if(p.getSex() == Person.Gender.Female) {
					// Is female
					female++;
				}
				if(p instanceof Drinker) {
					if(((Drinker) p).isDrunk()){
						// Is drunk
						drunk++;
					}
				}
			}
			// Now we have tallied the people involved
			if ((female/people >= 0.4) && (drunk/people >= 0.5)) {
				// Successful event
				return true;
			}
			else {
				return false;
			}
		}
	}

}

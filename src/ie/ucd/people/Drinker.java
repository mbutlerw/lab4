package ie.ucd.people;

import ie.ucd.items.*;

public class Drinker extends Person {
	double numberOfDrinks;

	public Drinker() {
		this.numberOfDrinks = 0; // Initialize not drunk
	}

	@Override
	public boolean drink(Drink arg0) {
		if (arg0 instanceof AlcoholicDrink) {
			// Is alcoholic drink
			numberOfDrinks++; // Increment number of drinks
		}
		else
		{
			// Isn't alcoholic
		}
		return true;
		// I took a guess that return is whether the drink was successful
	}

	@Override
	public boolean eat(Food arg0) {
		return true;
	}
	
	public boolean isDrunk() {
		if (this.numberOfDrinks > (this.getWeight() / 10))
		{
			// Drunk
			return true;
		}
		else 
		{
			// Not drunk
			return false;
		}
	}

}

package ie.ucd.people;

import ie.ucd.items.*;

public class NotDrinker extends Person {

	public NotDrinker() {
		
	}

	@Override
	public boolean drink(Drink arg0) {
		if (arg0 instanceof AlcoholicDrink) {
			// Is alcoholic drink
			return false; // Can't drink these
		}
		else
		{
			return true; // Isn't alcoholic
		}
	}

	@Override
	public boolean eat(Food arg0) {
		return false;
	}

}
